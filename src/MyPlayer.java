import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

        Move bestMove = search(gameBoard, 6, this.playerNumber);
        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber) {

        ArrayList<Move> moves = new ArrayList<Move>();
        int[][] tempBoard = gameBoard.getBoard();
        if(tempBoard[0][3] == 0 || tempBoard[1][3] == 0){
            Move thisMove = new Move(3);
            return thisMove;
        }
        // Try each possible move
        for (int i=0; i<Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);
        }

        // Pick the highest value move
        return this.getBestMove(moves);

    }

    public double heuristic(Board gameBoard, int playerNumber) {
        double total;
        double threeRow = checkThreeInRow(gameBoard, (playerNumber == 1 ? 1 : 2)) - checkThreeInRow(gameBoard, (playerNumber == 1 ? 2: 1));
        if(threeRow == 0.0){
            total = 0.0;
        } else if(threeRow < 0.0){
            if(threeRow == -1){
                total = -0.05;
            } else {
                total = -0.6 - (1/threeRow);
            }
        } else {
            if(threeRow == 1){
                total = 0.05;
            } else {
                total = 0.6 - (1/threeRow);
            }
        }
        double sevenTrap = checkSeven(gameBoard, (playerNumber == 1 ? 1 : 2)) - checkSeven(gameBoard, (playerNumber == 1 ? 2 : 1));
        double sevenTrapTotal;
        if(sevenTrap == 0.0){
            sevenTrapTotal = 0.0;
        } else if(sevenTrap < 0.0){
            sevenTrapTotal = -1 - (0.8 / sevenTrap);
        } else {
            sevenTrapTotal = 1 - (0.8 / sevenTrap);
        }

        total = (total + sevenTrapTotal)/2;
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0

        return total;
    }

    private int checkSeven(Board gameboard, int playerNumber){
        int[][] tempBoard = gameboard.getBoard();
        int count = checkSevenRightUp(tempBoard, playerNumber) + checkSevenLeftUp(tempBoard, playerNumber);
        count += checkSevenRightDown(tempBoard, playerNumber) + checkSevenLeftDown(tempBoard,playerNumber);
        return count;
    }

    private int checkSevenRightUp(int[][] gameboard, int playerNumber){
        int count = 0;
        for(int col = 0; col < 4; col++){
            for(int row = 0; row < 4; row++){
                if(gameboard[row][col] == playerNumber){
                    if(gameboard[row + 2][col] == playerNumber && gameboard[row + 2][col + 1] == playerNumber){
                        if(gameboard[row + 1][col + 1] == playerNumber && gameboard[row + 2][col + 2] == playerNumber){
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    private int checkSevenLeftUp(int[][] gameboard, int playerNumber){
        int count = 0;
        for(int col = 6; col > 2; col--){
            for(int row = 0; row < 4; row++){
                if(gameboard[row][col] == playerNumber){
                    if(gameboard[row + 2][col] == playerNumber && gameboard[row + 2][col - 1] == playerNumber){
                        if(gameboard[row + 1][ col - 1] == playerNumber && gameboard[row + 2][col - 2] == playerNumber){
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    private int checkSevenRightDown(int[][] gameboard, int playerNumber){
        int count = 0;
        for(int col = 0; col < 4; col++){
            for(int row = 6; row > 2; row--){
                if(gameboard[row][col] == playerNumber){
                    if(gameboard[row - 2][col] == playerNumber && gameboard[row - 2][col + 1] == playerNumber){
                        if(gameboard[row -1][col + 1] == playerNumber && gameboard[row - 2][col + 2] == playerNumber){
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    private int checkSevenLeftDown(int[][] gameboard, int playerNumber){
        int count = 0;
        for(int col = 6; col > 2; col--){
            for(int row = 6; row > 2; row--){
                if(gameboard[row][col] == playerNumber){
                    if(gameboard[row - 2][col] == playerNumber && gameboard[row -2][col - 1] == playerNumber){
                        if(gameboard[row -1][col -1] == playerNumber && gameboard[row -2][col - 2] == playerNumber){
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    private int checkThreeInRow(Board gameboard, int playerNumber){
        int[][] tempBoard = gameboard.getBoard();
        int count = numCols(tempBoard, playerNumber) + numRows(tempBoard, playerNumber) + numDiag(tempBoard, playerNumber);
        return count;

    }

    private int numDiag(int[][] tempBoard, int playerNumber){
        int count = 0;
        for(int col = 0; col < 5; col++){
            for(int row = 0; row < 5; row++){
                if(tempBoard[row][col] == playerNumber){
                    if(tempBoard[row + 1][col + 1] == playerNumber && tempBoard[row + 2][col + 2] == playerNumber){
                        count++;
                    }
                }
            }
        }
        for(int col2 = 0; col2 < 5; col2++){
            for(int row2 = 6; row2 > 1; row2--){
                if(tempBoard[row2][col2] == playerNumber){
                    if(tempBoard[row2 - 1][col2 + 1] == playerNumber && tempBoard[row2 - 2][col2 + 2] == playerNumber){
                        count++;
                    }
                }
            }
        }
        return count;
    }

    private int numCols(int[][] tempBoard, int playerNumber){
        int count = 0;
        for(int col = 0; col < 5; col++){
            for(int row = 0; row < 5; row++){
                if(tempBoard[row][col] == playerNumber){
                    if(tempBoard[row + 1][col] == playerNumber && tempBoard[row + 2][col] == playerNumber){
                        count++;
                        row += 2;
                    }
                }
            }
        }
        return count;
    }

    private int numRows(int[][] tempBoard, int playerNumber){
        int count = 0;
        for(int row = 0; row < 5; row++){
            for(int col = 0; col < 5; col++){
                if(tempBoard[row][col] == playerNumber){
                    if(tempBoard[row][col + 1] == playerNumber && tempBoard[row][col + 2] == playerNumber){
                        count++;
                        col += 2;
                    }
                }
            }
        }
        return count;
    }

    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);
        ArrayList<Move> same = new ArrayList<Move>();
        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
                same = new ArrayList<Move>();
            }
            if(cm.value == max){
                same.add(cm);
            }
        }
        if(same.size() > 1) {
            int bestDist;
            if (bestMove.move >= 3) {
                bestDist = bestMove.move - 3;
            } else {
                bestDist = 3 - bestMove.move;
            }
            for (Move sm : same) {
                 if(sm.move >= 3){
                     if(sm.move - 3 < bestDist){
                         bestMove = sm;
                         bestDist = (sm.move - 3);
                     }
                 } else {
                     if(3 - sm.move < bestDist){
                         bestMove = sm ;
                         bestDist = (3 - sm.move);
                     }
                 }
            }
        }

        return bestMove;
    }
}
